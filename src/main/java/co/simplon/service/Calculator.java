package co.simplon.service;

import co.simplon.Window;
import co.simplon.entity.Pizza;

public class Calculator {
  public static double result = 0;

  public static void calcSauce(int totalSauce) {
    result += totalSauce * 0.20;
  }

  public static void calcPizza(int pizzaNumber) {
    if (Window.cheese.getState()) {

      result = pizzaNumber * Pizza.price + 1.5;
    } else {
      result = pizzaNumber * Pizza.price;
    }


  }
}
