package co.simplon;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import co.simplon.service.Calculator;

public class SpiceeSpinner implements ChangeListener {
  int sauceAddition;


  @Override
  public void stateChanged(ChangeEvent e) {

    int sauceNumber = (int) Window.spiceeSauce.getValue();
    if (sauceNumber > sauceAddition) {
      Calculator.calcSauce(1);
    } else {
      Calculator.calcSauce(-1);
    }
    sauceAddition = sauceNumber;
    Window.totalPrice.setText("prix total : " + Calculator.result);

  }

}
