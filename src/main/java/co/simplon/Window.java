package co.simplon;

import java.awt.Checkbox;
import javax.swing.*;


public class Window extends JFrame {

  public static JLabel totalPrice = null;
  public static JTextField pizzaNumberInput = null;
  public static Checkbox cheese = null;
  public static JSpinner spiceeSauce = null;



  public Window() {
    JFrame f = new JFrame("Ma Pizza");// creating instance of JFrame


    JLabel title = new JLabel("Caisse Automatique");
    title.setBounds(150, 50, 200, 20);
    f.add(title);

    JLabel pizzaNumber = new JLabel("Nombre de Pizza");
    pizzaNumber.setBounds(50, 100, 200, 20);
    f.add(pizzaNumber);

    pizzaNumberInput = new JTextField();
    pizzaNumberInput.setBounds(50, 130, 100, 20);
    f.add(pizzaNumberInput);

    cheese = new Checkbox("supplément fromage");
    cheese.setBounds(40, 170, 200, 20);
    f.add(cheese);

    spiceeSauce = new JSpinner();
    spiceeSauce.setBounds(50, 200, 50, 20);
    f.add(spiceeSauce);
    spiceeSauce.addChangeListener(new SpiceeSpinner());

    JLabel spiceeSauceText = new JLabel("Nombre de sauces");
    spiceeSauceText.setBounds(100, 200, 200, 20);
    f.add(spiceeSauceText);

    JButton b = new JButton("Calculer le total");// creating instance of JButton
    b.setBounds(50, 250, 200, 20);// x axis, y axis, width, height
    f.add(b);// adding button in JFrame
    b.addActionListener(new ButtonPriceAction());

    totalPrice = new JLabel();
    totalPrice.setBounds(50, 400, 300, 20);
    f.add(totalPrice);



    f.setSize(400, 500);// 400 width and 500 height
    f.setLayout(null);// using no layout managers
    f.setVisible(true);// making the frame visible
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

  }


  // public double calcPizza(int pizzaNumber) {
  // return pizzaNumber * Pizza.price;
  // }


  // @Override
  // public void actionPerformed(ActionEvent e) {

  // // if (pizzaNumberInput.getText().matches("^[0-9]+$")) {
  // // totalPrice.setText("prix total : " +
  // calcPizza(Integer.valueOf(pizzaNumberInput.getText())));
  // // } else {
  // // totalPrice.setText("l'entrée n'est pas valide");
  // // }

  // try {
  // totalPrice.setText(
  // "prix total : " + calcPizza(Integer.valueOf(pizzaNumberInput.getText())) + " euro");
  // } catch (NumberFormatException exception) {
  // totalPrice.setText("l'entrée n'est pas valide");
  // }

  // }
}
