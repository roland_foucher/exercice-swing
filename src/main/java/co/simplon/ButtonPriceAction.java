package co.simplon;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import co.simplon.service.Calculator;


public class ButtonPriceAction implements ActionListener {



  @Override
  public void actionPerformed(ActionEvent e) {

    try {

      int pizzaNumberInput = Integer.valueOf(Window.pizzaNumberInput.getText());
      Calculator.calcPizza(pizzaNumberInput);
      int sauceNumber = (int) Window.spiceeSauce.getValue();
      Calculator.calcSauce(sauceNumber);
      Window.totalPrice.setText("prix total : " + Calculator.result);

    } catch (NumberFormatException exception) {
      Window.totalPrice.setText("l'entrée n'est pas valide");


    }
  }

}
